const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

var dssv = [];

var dssvJson = localStorage.getItem("DSSV_LOCALSTORAGE");

if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDSSV(dssv);
}

function themSV() {
  var newSv = layThongTinTuForm();

  var isValid =
    validation.kiemTraRong(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) &&
    validation.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 kí tự",
      4,
      4
    );

  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    );
  isValid =
    validation.kiemTraRong(
      newSv.ten,
      "spanEmailSV",
      "Email sinh viên không được rỗng"
    );
  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.ten,
      "spanMatKhau",
      "Mật khẩu sinh viên không được rỗng"
    );

  isValid = isValid && validation.kiemTraEmail(newSv.email, "spanEmailSV", "Email của bạn không đúng cú pháp")

  if (isValid) {
    dssv.push(newSv);

    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV_LOCALSTORAGE", dssvJson);
    renderDSSV(dssv);
    1;
  }

}

function xoaSinhVien(id) {
  console.log(id);

  var index = timKiemViTri(id, dssv);
  if (index != -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}

function capNhatThongTin() {
  var updateSV = layThongTinTuForm();
  var maSv = updateSV.ma;
  var index = timKiemViTri(maSv, dssv);
  if (index != 1) {
    dssv[index] = updateSV;
    localStorage.setItem("DSSV_LOCALSTORAGE", JSON.stringify(dssv));
    renderDSSV(dssv);
  }
  document.getElementById("txtMaSV").disabled = false;
};


document.getElementById("btnSearch").addEventListener("click", function () {
  searchUser();

})


function reset() {
  document.getElementById("tbodySinhVien")[0].reset();
}
